from __future__ import annotations

from time import gmtime, strftime
from typing import Any, Generator, Union

import numpy as np
from ase.db.core import T2000, YEAR, Database
from ase.db.row import AtomsRow
from ase.formula import Formula

RawRow = tuple[list[str],
               np.ndarray,
               dict[str, Union[int, float, str]]]


def extract_data_from_db_row(row: AtomsRow) -> RawRow:
    """Get data needed for IndexDB."""
    kvp = row.key_value_pairs
    if 'energy' in row:
        kvp['energy'] = row.energy  # type: ignore
    return row.symbols, row.pbc, kvp


def db2rows(db: Database) -> Generator[RawRow, None, None]:
    """Extract data from all rows of an ase.db database."""
    for row in db.select():
        yield extract_data_from_db_row(row)


def get_optimade_things(formula: Formula, pbc: np.ndarray) -> dict:
    """Collect some OPTIMADE stuff."""
    _, reduced, num = formula.stoichiometry()
    count = reduced.count()

    # Alphapetically sorted:
    reduced = Formula.from_dict({symbol: count[symbol]
                                 for symbol in sorted(count)})

    # Elements with highest proportion should appear first:
    c = ord('A')
    dct = {}
    for n in sorted(count.values(), reverse=True):
        dct[chr(c)] = n
        c += 1
    anonymous = Formula.from_dict(dct)

    return {
        'chemical_formula_descriptive': f'{formula:hill}',
        'chemical_formula_reduced': f'{reduced}',
        'chemical_formula_anonymous': f'{anonymous}',
        'chemical_formula_hill': f'{formula:hill}',
        'nsites': num * sum(count.values()),
        'nelements': len(count),
        'nperiodic_dimensions': int(sum(pbc))
    }


def row2dict(row: AtomsRow) -> dict[str, Any]:
    """Convert ase.db.database row to OPTIMADE dictionary."""
    count = dict(sorted(row.count_atoms().items()))
    formula = Formula.from_dict(count)
    t = row.mtime * YEAR + T2000
    last_modified = strftime('%Y-%m-%dT%H:%M:%SZ', gmtime(t))
    dct = {
        'cartesian_site_positions': row.positions.tolist(),
        'species_at_sites': row.symbols,
        'species': [{'name': symbol,
                     'chemical_symbols': [symbol],
                     'concentration': [1.0]}
                    for symbol in count],
        'lattice_vectors': row.cell.tolist(),
        'dimension_types': row.pbc.astype(int).tolist(),
        'last_modified': last_modified,
        'elements': list(count),
        'elements_ratios': [n / row.natoms for n in count.values()],
        'structure_features': [],
        'id': str(row.id)}
    dct |= get_optimade_things(formula, row.pbc)

    return dct
