from __future__ import annotations
import operator
from typing import Any, Optional

import numpy as np
from ase.data import atomic_numbers
from ase.formula import Formula
from sqlalchemy import (Column, Float, ForeignKey, Integer, MetaData, String,
                        Table, and_, create_engine, not_, or_, select)
from sqlalchemy.engine.base import Engine
from sqlalchemy.sql.elements import ColumnElement as Clauses

from oase.asedata import get_optimade_things, RawRow

OPS = {
    '=': operator.eq,
    '!=': operator.ne,
    '>': operator.gt,
    '<': operator.lt,
    '>=': operator.ge,
    '<=': operator.le}

SPECIAL = {
    'nsites',
    'nelements',
    'nperiodic_dimensions',
    'chemical_formula_descriptive',
    'chemical_formula_reduced',
    'chemical_formula_anonymous',
    'chemical_formula_hill'}


class IndexDB:
    """Database for querying an ase.db database."""
    def __init__(self, engine: Engine | None = None):
        """Create OPTIMADE index-database for ase.db database."""
        self.metadata = create_metadata()
        if engine is None:
            engine = create_engine('sqlite:///:memory:', echo=not True)
            self.metadata.create_all(engine)
        self.engine = engine
        self.kinds: dict[str, str] = {}
        self.structures = self.metadata.tables['structures']
        self.species = self.metadata.tables['species']
        self.structure_features = self.metadata.tables['structure_features']
        self.nstructures = 0

    def insert(self,
               rows: list[RawRow]) -> None:
        """Insert rows."""
        structures = []
        species = []
        key_value_data: dict[str, list[dict]] = {
            'strings': [],
            'ints': [],
            'floats': []}
        tables = self.metadata.tables
        for id, (symbols, pbc, keywords) in enumerate(rows, start=1):
            formula = Formula.from_list(symbols)
            count = formula.count()
            structures.append({'_id': id} |
                              get_optimade_things(formula, pbc))
            for symbol, n in count.items():
                species.append({'_id': id,
                                'Z': atomic_numbers[symbol],
                                'count': n})
            keywords = keywords | {'id': str(id)}
            for key, value in keywords.items():
                if isinstance(value, str):
                    kind = 'strings'
                elif isinstance(value, int):
                    kind = 'ints'
                elif isinstance(value, float):
                    kind = 'floats'
                else:
                    raise ValueError
                k = self.kinds.get(key)
                if k is not None:
                    if k != kind:
                        raise ValueError
                else:
                    self.kinds[key] = kind
                key_value_data[kind].append({'_id': id,
                                             'name': key,
                                             'value': value})
            self.nstructures += 1

        with self.engine.connect() as conn:
            with conn.begin():
                conn.execute(tables['structures'].insert(), structures)
                conn.execute(tables['species'].insert(), species)
                for name, data in key_value_data.items():
                    conn.execute(tables[name].insert(), data)


    def select(self,
               node: Optional[Any] = None) -> Clauses:
        """Create SELECT SQL-statement."""
        if node is None:
            return range(1, self.nstructures + 1)

        if len(node) == 3:
            n1, n2, n3 = node
            if n2[0] == 'OPERATOR' and n3[0] == 'IDENTIFIER':
                key = n3[1]
                op = n2[1]
                value = n1
                return self.compare(key, op, value)
            raise ValueError

        n1, n2 = node
        if n1 == 'OR':
            return or_(*(self.select(value) for value in n2))
        if n1 == 'AND':
            return and_(*(self.select(value) for value in n2))
        if n1 == ('NOT', 'NOT'):
            return not_(self.select(n2))
        if n1[0] == 'IDENTIFIER':
            key = n1[1]
            *n3, n4 = n2
            name = ' '.join(n[0] for n in n3)
            if name == 'HAS':
                value = n4
                return self.has(key, value)
            if name == 'HAS ALL':
                values = n4 if isinstance(n4, list) else [n4]
                return and_(*(self.has(key, value) for value in values))
            if name == 'HAS ANY':
                values = n4 if isinstance(n4, list) else [n4]
                return or_(*(self.has(key, value) for value in values))
            if name in {'ENDS WITH', 'ENDS'}:
                value = n4
                assert isinstance(value, str)
                return self.endswith(key, value)
            if name in {'STARTS WITH', 'STARTS'}:
                value = n4
                assert isinstance(value, str)
                return self.startswith(key, value)
            if name == 'CONTAINS':
                value = n4
                assert isinstance(value, str)
                return self.contains(key, value)
            if n3[0][0] == 'OPERATOR':
                op = n3[0][1]
                value = n4
                return self.compare(key, op, value)

        raise ValueError

    def has(self,
            key: str,
            value: str) -> Clauses:
        """Implemention of "elements HAS symbol"."""
        if key in {'elements', 'species_at_sites'}:
            Z = atomic_numbers[value]
            return self.structures.c._id.in_(select(self.species.c._id).
                                             where(self.species.c.Z == Z))
        assert key == 'structure_features'
        return and_(self.structure_features.c._id == self.structures.c._id,
                    self.structure_features.c.name == value)

    def compare(self,
                key: str,
                op: str,
                value: int | float | str) -> Clauses:
        """Implemention of "key <op> value"."""
        cmp = OPS[op]
        if key in SPECIAL:
            return cmp(self.structures.c[key], value)
        table = self.metadata.tables[self.kinds.get(key, 'ints')]
        return and_(table.c._id == self.structures.c._id,
                    table.c.name == key,
                    cmp(table.c.value, value))

    def endswith(self,
                 key: str,
                 value: str) -> Clauses:
        """Implemention of "key <op> value"."""
        if key in SPECIAL:
            return self.structures.c[key].endswith(value)
        table = self.metadata.tables['strings']
        return and_(table.c._id == self.structures.c._id,
                    table.c.name == key,
                    table.c.value.endswith(value))

    def startswith(self,
                   key: str,
                   value: str) -> Clauses:
        """Implemention of "key <op> value"."""
        if key in SPECIAL:
            return self.structures.c[key].startswith(value)
        table = self.metadata.tables['strings']
        return and_(table.c._id == self.structures.c._id,
                    table.c.name == key,
                    table.c.value.startswith(value))

    def contains(self,
                 key: str,
                 value: str) -> Clauses:
        """Implemention of "key <op> value"."""
        if key in SPECIAL:
            return self.structures.c[key].contains(value)
        table = self.metadata.tables['strings']
        return and_(table.c._id == self.structures.c._id,
                    table.c.name == key,
                    table.c.value.contains(value))

    def execute(self, clauses: Clauses) -> list[int]:
        """Execute SQL-SELECT statement."""
        selection = select(self.structures.c._id).where(clauses)
        with self.engine.connect() as conn:
            result = [row[0] for row in conn.execute(selection)]
        return result


def create_metadata() -> MetaData:
    """Create SQL metadata."""
    metadata = MetaData()
    Table('structures',
          metadata,
          Column('_id', Integer, primary_key=True),
          *(Column(key,
                   Integer
                   if isinstance(value, int) else
                   String)  # type: ignore
            for key, value
            in get_optimade_things(Formula('H'), np.zeros(3, bool)).items()))
    Table('species',
          metadata,
          Column('_id', Integer, ForeignKey('structures._id')),
          Column('Z', Integer),
          Column('n', Integer))
    Table('ints',
          metadata,
          Column('_id', Integer, ForeignKey('structures._id')),
          Column('name', String),
          Column('value', Integer))
    Table('floats',
          metadata,
          Column('_id', Integer, ForeignKey('structures._id')),
          Column('name', String),
          Column('value', Float))
    Table('strings',
          metadata,
          Column('_id', Integer, ForeignKey('structures._id')),
          Column('name', String),
          Column('value', String))
    Table('structure_features',
          metadata,
          Column('_id', Integer, ForeignKey('structures._id')),
          Column('name', String))
    return metadata
