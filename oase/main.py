from __future__ import annotations

import argparse

from ase.db import connect

from oase.app import make_app
from oase.asedata import db2rows
from oase.indexdb import IndexDB
from oase.parser import create_parse_function


def main(arguments: list[str] | None = None) -> int:
    """Command line tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--add-db')
    parser.add_argument('-i', '--index')
    parser.add_argument('-q', '--query')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-s', '--serve', action='store_true')
    args = parser.parse_args(arguments)

    indexdb = IndexDB(args.index)
    if args.add_db:
        db = connect(args.add_db)
        with db:
            indexdb.insert([row for row in db2rows(db)])

    parse = create_parse_function()
    if args.query:
        tree = parse(args.query)
        if args.verbose:
            print(tree)
        selection = indexdb.select(tree)
        rowids = indexdb.execute(selection)
        print(rowids)

    if args.serve:
        app = make_app(indexdb, db.get)
        app.run()

    return 0
