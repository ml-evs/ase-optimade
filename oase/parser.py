from __future__ import annotations
from lark.lexer import Token  # type: ignore
from lark.tree import Tree  # type: ignore
from typing import Any, Callable
import functools
from optimade.filterparser import LarkParser  # type: ignore


def parse_lark_tree(node: Tree | Token) -> Any:
    """Convert Lark tree to simple data structure.

    See examples in ``parser_test.py``.
    """
    if isinstance(node, Token):
        if node.type == 'SIGNED_INT':
            return int(node.value)
        if node.type == 'SIGNED_FLOAT':
            return float(node.value)
        if node.type == 'ESCAPED_STRING':
            return node.value[1:-1]
        return (node.type, node.value)
    children = [parse_lark_tree(child)  # type: ignore
                for child in node.children]
    if len(children) == 1:
        return children[0]
    if node.data == 'expression':
        return ('OR', children)
    if node.data == 'expression_clause':
        return ('AND', children)
    return children


def parse(filter: str, parser: LarkParser) -> Any:
    """Parse OPTIMADE filter string to simple data structure"""
    return parse_lark_tree(parser.parse(filter))


def create_parse_function() -> Callable[[str], Any]:
    """Create a parser function."""
    parser = LarkParser(version=(1, 0, 0))
    return functools.partial(parse, parser=parser)
