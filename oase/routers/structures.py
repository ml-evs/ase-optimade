from fastapi import APIRouter, Depends, Request
from typing import Set, Union, Any, Dict
from pathlib import Path
from optimade.filterparser import LarkParser

from optimade.models import (
    StructureResource,
    StructureResponseMany,
    StructureResponseOne,
)
from optimade.server.config import CONFIG
from optimade.server.mappers import StructureMapper
from optimade.server.query_params import EntryListingQueryParams, SingleEntryQueryParams
from optimade.server.routers.utils import get_entries, get_single_entry
from optimade.server.entry_collections import EntryCollection
from optimade.server.schemas import ERROR_RESPONSES
from oase.asedata import extract_data_from_db_row, db2rows
from oase.parser import parse_lark_tree
from oase.tests import make_index_db


router = APIRouter(redirect_slashes=True)

class ASEEntryCollection(EntryCollection):

    def __init__(self):
        self.indexdb, self.ase_db = make_index_db(Path(__file__).parent.parent / "databases" / "testasdfas.db")
        # self.ase_db = connect(path)
        # with self.ase_db as db:
        #     print(f"Inserting from {path}")
        #     self.indexdb.insert([row for row in db2rows(db)])

        self.parser = LarkParser()
        self.resource_cls = StructureResource
        self.resource_mapper = StructureMapper
        self.transformer = None

        self.provider_prefix = CONFIG.provider.prefix
        self.provider_fields = [
            field if isinstance(field, str) else field["name"]
            for field in CONFIG.provider_fields.get(self.resource_mapper.ENDPOINT, [])
        ]

        self._all_fields: Set[str] = set()

    def transform(self, filter_: str):
        return parse_lark_tree(self.parser.parse(filter_))


    def _run_db_query(self, criteria, single_entry):
       
        data_returned = None
        limit = criteria.pop("limit", None)
        skip = criteria.pop("skip", 0)
        if criteria["filter"]:
            selection = self.indexdb.select(criteria["filter"])
            rowids = self.indexdb.execute(selection)
            results = [extract_data_from_db_row(row) for row in rowids]
        else:
            results = [extract_data_from_db_row(row) for row in self.indexdb.select()]
            data_returned = 5

        if not single_entry:
            if data_returned is None:
                data_returned = len(rowids)
            results = results[skip:limit]
            more_data_available = len(results) + skip < data_returned
        else:
            data_returned = 1
            more_data_available = False

        return results, data_returned, more_data_available

    def __len__(self):
        return self.indexdb.nstructures
    
    def count(self, query):
        pass

    def insert(self):
        pass


    def handle_query_params(self, params: Union[EntryListingQueryParams, SingleEntryQueryParams]) -> Dict[str, Any]:
        filter = self.transform(params.filter)
        params.filter = False
        query = super().handle_query_params(params)
        query["filter"] = filter
        return query


structures_coll = ASEEntryCollection()

@router.get(
    "/structures",
    response_model=StructureResponseMany,
    response_model_exclude_unset=True,
    tags=["Structures"],
    responses=ERROR_RESPONSES,
)
def get_structures(
    request: Request, params: EntryListingQueryParams = Depends()
) -> StructureResponseMany:
    return get_entries(
        collection=structures_coll,
        response=StructureResponseMany,
        request=request,
        params=params,
    )


@router.get(
    "/structures/{entry_id:path}",
    response_model=StructureResponseOne,
    response_model_exclude_unset=True,
    tags=["Structures"],
    responses=ERROR_RESPONSES,
)
def get_single_structure(
    request: Request, entry_id: str, params: SingleEntryQueryParams = Depends()
) -> StructureResponseOne:
    return get_single_entry(
        collection=structures_coll,
        entry_id=entry_id,
        response=StructureResponseOne,
        request=request,
        params=params,
    )
