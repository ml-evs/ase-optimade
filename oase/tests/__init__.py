from ase import Atoms
from ase.db import connect
from ase.calculators.singlepoint import SinglePointCalculator
from oase.asedata import extract_data_from_db_row
from oase.indexdb import IndexDB


def make_index_db(path):
    db = connect(path)
    h2 = Atoms('H2')
    h2.calc = SinglePointCalculator(h2, energy=-1.0)
    db.write(h2,
             hmm='a123')
    db.write(Atoms('CO', pbc=True),
             x=7,
             hmm='hi!')
    indexdb = IndexDB()
    indexdb.insert([extract_data_from_db_row(row)
                    for row in db.select()])
    return indexdb, db
