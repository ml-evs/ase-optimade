from oase.app import App, get_config, make_test_app, get_pages


def test_make_app(tmp_path):
    make_test_app(str(tmp_path / 'test.db'))


def test_cfg(tmp_path, monkeypatch):
    monkeypatch.delenv('OASE_CONFIG_FILE', raising=False)
    cfg = get_config()
    assert len(cfg) == 2

    path = tmp_path / 'cfg.json'
    monkeypatch.setenv('OASE_CONFIG_FILE', str(path))
    path.write_text('{}')
    cfg = get_config()
    assert not cfg


def test_app(indexdbs):
    indexdb, asedb = indexdbs
    app = App(indexdb, asedb.get)

    text = app.index()
    assert text.split().count('<tr>') == 3

    assert app.versions() == 'version\n1\n'

    app.info_structures()
    dct = app.info()
    assert 'meta' in dct

    dct = app.links()
    assert 'meta' in dct

    dct2 = app.structures()
    assert len(dct2['data']) == 2

    dct = app.structures_id('1')
    assert dct['data'] == dct2['data'][0]

    app._query = {'filter': 'elements HAS "H"'}
    dct = app.structures()
    assert len(dct['data']) == 1

    app._query['response_fields'] = 'id,nsites'
    dct = app.structures()
    assert len(dct['data'][0]['attributes']) == 2


def test_paging():
    assert get_pages(9, 36, 2) == [
        (8, 'previous'), (10, 'next'),
        (0, '1-2'), (1, '3-4'),
        (-1, '...'),
        (7, '15-16'), (8, '17-18'), (9, '19-20'), (10, '21-22'), (11, '23-24'),
        (-1, '...'),
        (17, '35-36'), (18, '37-36')]
