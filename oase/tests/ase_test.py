import numpy as np
import pytest
from ase import Atoms
from ase.db import connect
from ase.formula import Formula
from oase.asedata import get_optimade_things, row2dict
from oase.main import main


@pytest.fixture
def asedb_path(tmp_path):
    path = tmp_path / 'test.db'
    db = connect(path)
    with db:
        db.write(Atoms(), x=42)
    return path


def test_ase(asedb_path):
    main(['-a', str(asedb_path), '-q', 'x=42'])


def test_optimade_dict(asedb_path):
    row = connect(asedb_path).get(x=42)
    dct = row2dict(row)
    print(dct)


def test_formula_stuff():
    dct = get_optimade_things(Formula('Se2V'), np.zeros(3, bool))
    print(dct)
    assert dct['chemical_formula_reduced'] == 'Se2V'
    assert dct['chemical_formula_anonymous'] == 'A2B'
