import pytest
from optimade.filterparser import LarkParser  # type: ignore

from oase.tests import make_index_db


@pytest.fixture
def parser() -> LarkParser:
    return LarkParser(version=(1, 0, 0))


@pytest.fixture(scope='session')
def indexdbs(tmp_path_factory):
    idb, asedb = make_index_db(tmp_path_factory.mktemp('db') / 'test.db')
    return idb, asedb
