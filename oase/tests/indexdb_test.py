from oase.indexdb import IndexDB
import pytest


def test_errors():
    idb = IndexDB()
    with pytest.raises(ValueError):
        idb.insert([([], {'x': 1j})])
    with pytest.raises(ValueError):
        idb.insert([([], {'x': 1}),
                    ([], {'x': '1'})])
