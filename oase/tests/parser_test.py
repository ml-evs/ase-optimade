import pytest

from oase.parser import parse_lark_tree, create_parse_function


@pytest.mark.parametrize(
    'query, expected',
    [('x HAS ALL "Au", "Cu"',
      [('IDENTIFIER', 'x'),
       [('HAS', 'HAS'), ('ALL', 'ALL'), ['Au', 'Cu']]]),
     ('x=1 AND y=2',
      ('AND',
       [[('IDENTIFIER', 'x'), [('OPERATOR', '='), 1]],
        [('IDENTIFIER', 'y'), [('OPERATOR', '='), 2]]])),
     ('x=1 OR y=2',
      ('OR',
       [[('IDENTIFIER', 'x'), [('OPERATOR', '='), 1]],
        [('IDENTIFIER', 'y'), [('OPERATOR', '='), 2]]])),
     ('NOT x=1',
      [('NOT', 'NOT'),
       [('IDENTIFIER', 'x'), [('OPERATOR', '='), 1]]])])
def test_parser(query, expected, parser):
    tree = parser.parse(query)
    print(tree)
    print()
    result = parse_lark_tree(tree)
    print(result)
    assert result == expected


def test_create_lark_parser():
    parse = create_parse_function()
    assert parse('x=2') == [('IDENTIFIER', 'x'), [('OPERATOR', '='), 2]]
